<?php
//Copyright 2018,2021,2023 Tobias Grundmann
//
//This file is part of Windenfahrerplan.
//
//Windenfahrerplan is free software: you can redistribute it and/or modify
//it under the terms of version 3 of the GNU General Public License as
//published by the Free Software Foundation
//
//Windenfahrerplan is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Windenfahrerplan.  If not, see <http://www.gnu.org/licenses/>.

error_reporting(E_ALL);

require_once 'plan.php';

function printPost() {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                echo "<div id='post' style='border:solid;background-color:white;display:inline-block;float:right;padding: 0.5em'>";
                foreach($_POST as $key => $value) {
                        printf("<p>POST [%s] => [%s]</p>",$key,$value);
                }
                echo "</div>";
        }
}

function sanitize($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
}

$plan = Plan::laden();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
        foreach($_POST as $key => $value) {
                error_log("POST [" . $key . "] => [" . $value . "]",0);
        }
        if (isset($_POST["action"])) {
                $action         = isset($_POST["action"])         ? sanitize($_POST["action"])         : null;
                $fahrerName     = isset($_POST["fahrerName"])     ? sanitize($_POST["fahrerName"])     : null;
                $fahrerId       = isset($_POST["fahrerId"])       ? sanitize($_POST["fahrerId"])       : null;
                $windenfahrerGS = isset($_POST["windenfahrerGS"]) ? sanitize($_POST["windenfahrerGS"]) : null;
                $windenfahrerHG = isset($_POST["windenfahrerHG"]) ? sanitize($_POST["windenfahrerHG"]) : null;
                $EWF            = isset($_POST["EWF"])            ? sanitize($_POST["EWF"])            : null;
                $datum          = isset($_POST["datum"])          ? sanitize($_POST["datum"])          : null;
                $spaet          = isset($_POST["spaet"])          ? sanitize($_POST["spaet"])          : null;
                $frueh          = isset($_POST["frueh"])          ? sanitize($_POST["frueh"])          : null;
                $hinweisText    = isset($_POST["hinweisText"])    ? sanitize($_POST["hinweisText"])    : null;
                $resourceID     = isset($_POST["resourceID"])     ? sanitize($_POST["resourceID"])     : null;
                $resourceValue  = isset($_POST["resourceValue"])  ? sanitize($_POST["resourceValue"])  : null;
                
                if ($action == "neuerFahrer") {
                        $plan->neuerFahrer($fahrerName,$windenfahrerGS,$windenfahrerHG,$EWF);
                } elseif ($action == "fahrerAendern") {
                        $plan->fahrerAendern($fahrerId,$fahrerName,$windenfahrerGS,$windenfahrerHG,$EWF);
                } elseif ($action == "fahrerLoeschen") {
                        $plan->fahrerLoeschen($fahrerId);
                } elseif ($action == "anmelden") {
                        $plan->update($fahrerId,$datum,$spaet,$frueh);
                } elseif ($action == "hinweis") {
                        $plan->hinweis($hinweisText);
                } elseif ($action == "staticResourceChange") {
                        $plan->staticResourceChanged($resourceID,$resourceValue);
                }
        } elseif(isset($_POST["Zufallswerte"])) {
                //$plan = new Plan();
                //$plan->random();
        }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
    <head>
        <!-- Copyright 2018 Tobias Grundmann -->
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1"/>
        <title>Badminton Spielplan</title>
        <link rel="stylesheet" type="text/css" href="css/styles.css"/>
<!--
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400&display=swap" rel="stylesheet">
-->
        <!--
        <link rel="icon" href="plan.gif" type="image/gif">
        -->
    </head>

    <body>
        <div>
             <div class="headerDiv">
                 <img src="plan.gif" alt="Logo"/>
             </div>
             <div class="headerDiv">
                 <h1> Badminton Spielplan</h1>
             </div>
        </div>

        <div id="tableContainer">
               
        <table id="bereitschaftTable" class="bereitschaftTable">
        <?php
        echo "<tr class='datumszeile'>";
        echo "<th style='width: 1.5em'>&nbsp;</th>";
        echo "<td style='width: 12em'></td>";
        //echo "<th>&nbsp;</th>";
        echo "<th class='fahrerFeature' style='width: 2.5em'>&nbsp;</th>";
        for ($i=0; $i < count($plan->referenzBereitschaft->tage); $i++) {
                echo "<th class='tag'>";
                $tag = $plan->referenzBereitschaft->tage[$i];
                echo $tag->dm();
                echo "</th>";
        }
        for ($i=0; $i < count($plan->bereitschaften); $i++) {
                $fahrer = $plan->bereitschaften[$i]->fahrer;
                echo "<tr class='fahrerzeile'>";
                echo "<th id='menu_" . $fahrer->id . "'><span class='active fahrerMenu'>&#x2699;</span></th>";
                echo "<th class='fahrer' id='" . $fahrer->id . "'>";
                //echo "<span class='active fahrerMenu'>&#x25BE;</span>";
                echo "<div class='fahrerDiv'>";
                if ($i) {
                        echo "<span class='active arrow'>&uarr;</span>";
                } else {
                        echo "<span class='no_arrow_span'></span>";
                }
                echo ' <!--<span style="font-size:40%"> (' . $plan->bereitschaften[$i]->score() . ')</span> -->' .
                         "<span>" . $fahrer->renderContent() . '</span>';
                echo "</div>";
                echo "</th>";
                echo "<td class='fahrerFeature'>" . $fahrer->renderFeature() . "</td>";
                for ($j=0; $j < count($plan->bereitschaften[$i]->tage); $j++) {
                        $tag =  $plan->bereitschaften[$i]->tage[$j];
                        echo "<td class='bereitschaft' id='$tag->id'>";
                        echo $plan->bereitschaften[$i]->tage[$j]->renderContent();
                        echo "</td>";
                }
                echo "</tr>\n";
        }
        ?>
        </table>
        </div>
        <div>
        <button id="neuerFahrerButton" class="button">Neuer Teilnehmer</button>
        </div>
        <?php
             //echo '<form method="post" action="windenfahrerplan.php">';
             //echo '       <div id="zufallButton"><input type="submit" name="Zufallswerte" value="Zufallswerte" class="button"/></div>';
             //echo '</form>';
        ?>
        <div id="legende">
        <p/>
        <hr/>
        <p/>
        <table id="legendeTable" style="overflow-x:auto; float:left;">
                        <tr><th colspan = '2'>&nbsp;  </th><th>verfügbar       </th></tr>
                        <tr><th class='dropdownOption_yes_frueh' colspan = '2'>&#x2713;</th><th class='dropdownOption_yes_frueh'>Ich bin dabei      </th></tr>
                        <tr><th class='dropdownOption_maybe_frueh' colspan = '2'>?       </th><th class='dropdownOption_maybe_frueh'>Wei&szlig; nicht   </th></tr>
                        <tr><th class='dropdownOption_no_frueh' colspan = '2'>&times; </th><th class='dropdownOption_no_frueh'>keine Zeit</th></tr>
        </table>
        <table id="legendeTable" style="border-style:hidden; width:100px; text-align:right; float:left;">
                <tr><th style="border-style:hidden;font-weight: normal;">Ersatz für:</th></tr>
        </table>
        <table id="legendeTable" style="overflow-x:auto; float:left;">
                        <tr><th colspan = '2'>&nbsp; An</th><th>Anna</th></tr>
                        <tr><th colspan = '2'>&nbsp; So</th><th>Sonja</th></tr>
                        <tr><th colspan = '2'>&nbsp; Lu</th><th>Lukas</th></tr>
                        <tr><th colspan = '2'>&nbsp; Ma</th><th>Marco</th></tr>
        </table>

        <p style="clear:both"><small>
                &copy; Tobias Grundmann, Christian Motschke<br/>
                Lizenz: <a href="https://www.gnu.org/licenses/gpl-3.0">GPL 3.0</a><br/>
                <a href="https://gitlab.com/sports-and-fun/spielplan">Spielplan Download</a>
        </small></p>
        </div>
        <hr/>

        <div id="modal" class="modal">
            <div id="neuerFahrerPopup" class="overlayHidden">
                  <div id="closeNeuerFahrer" class="close active"><span>&times;</span></div><br/>
                  <?php echo '<form id="neuerFahrerForm" method="post" action="' . basename($_SERVER['SCRIPT_FILENAME']) . '">'; ?>
                      <fieldset>
                      <input type="hidden" name="action" value="neuerFahrer"/>
                      <input type="hidden" name="fahrerId" value=""/>
                      <legend id="fahrerPopupH">Neuer Teilnehmer</legend>
                      <b>
                      <label>Name             <input type="text"     name="fahrerName"/></label><br/>
                      </b>
                      </fieldset>
                      <div>
                          <input type="submit" value="abschicken" class="button"/>
                          <input id="fahrerLoeschen" type="submit" value="Teiln. l&ouml;schen" class="button"/>
                      </div>
                  </form>
            </div>

            <div id="hinweisPopup" class="overlayHidden">
                  <div id="closeHinweis" class="close active"><span>&times;</span></div><br/>
                  <?php echo '<form id="hinweisForm" method="post" action="' . basename($_SERVER['SCRIPT_FILENAME']) . '">'; ?>
                      <fieldset>
                      <input type="hidden" name="action" value="hinweis"/>
                      <legend id="hinweisePopupH">Max. <?php echo $plan->hinweisMaxLen ?> Zeichen. (<span id="charcount"></span>)</legend>
                                                                                                                                                                                
                      <b>
                      <!--<label>Text: <input type="text"     name="hinweisText"/></label><br/>-->
                      <textarea cols="50" rows="5" name="hinweisText" id="hinweisText" maxlength="<?php echo $plan->hinweisMaxLen ?>"></textarea>
                      </fieldset>
                      </b>
                                <div>
                                         <input type="submit" value="abschicken" class="button"/>
                                </div>
                                                                                      
                  </form>
            </div>
            <div id="fahrerAnmeldungPopup" class="overlayHidden">
            <?php echo '<form id="anmeldeform" method="post" action="' . basename($_SERVER['SCRIPT_FILENAME']) . '">'; ?>
                    <fieldset>
                    <input type="hidden" name="fahrerId" value=""/>
                    <input type="hidden" name="datum"  value=""/>
                    <input type="hidden" name="action" value="anmelden"/>
                    <input type="hidden" name="frueh" value=""/>
                    <input type="hidden" name="spaet" value=""/>
                    </fieldset>
                    </form>
            </div>
        </div>

        <script type="text/javascript">
        <?php
            echo "var jsonPlanStr='" . $plan->asJson(). "';";
        ?>
        </script>
        <script type="text/javascript" src="plan.js">
        </script>
    </body>

</html>
