<?php
//Copyright 2018,2021,2023 Tobias Grundmann
//
//This file is part of Windenfahrerplan.
//
//Windenfahrerplan is free software: you can redistribute it and/or modify
//it under the terms of version 3 of the GNU General Public License as
//published by the Free Software Foundation
//
//Windenfahrerplan is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Windenfahrerplan.  If not, see <http://www.gnu.org/licenses/>.

error_reporting(E_ALL);

class Fahrer {
        public $name;
        public $id;
        public $windenfahrerGS = false;
        public $windenfahrerHG = false;
        public $EWF = false;
        public $lastAction;
        public $editable;

        public function __construct($name,$id) {
                $this->name = $name;
                $this->id = 'f_' . $id;
        }

        public function lastAction($datum) {
                $this->lastAction = $datum->format("Y-m-d");
        }
        public function lastActionAsDate () {
                return DateTime::createFromFormat("Y-m-d",$this->lastAction);
        }
        public function renderFeature() {
                if ($this->EWF) {
                        return "EWF";
                }
                if ($this->windenfahrerHG) {
                        return "WD";
                }
                if ($this->windenfahrerGS) {
                        return "W";
                }
                return "";
        }
        public function renderContent() {
                $GS = $this->windenfahrerGS ? "G" : "-";
                $HG = $this->windenfahrerHG ? "D" : "-";
                $EWF = $this->EWF ? "E" : "-";
                $name = $this->name;
                //$debug = "<small>|" . $this->id . " (" . $GS . "," . $HG . "," . $EWF . "," . $this->lastAction . ")</small>";
                $debug = '';
                return $name . $debug;
        }
}

class Tag {
        public function __construct($datum) {
                $this->datum = $datum->format("Y-m-d");
        }
        public $id;
        public $datum;
        public $frueh = "_";
        public $spaet = "_";

        public function setId ($id) {
                $this->id = "tag_" . $id;
        }
        public function asDate () {
                return DateTime::createFromFormat("Y-m-d",$this->datum);
        }
        public function dm () {
                $wochentage = array( 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' );
                $d = $this->asDate();
                return $wochentage[$d->format("w")] . " " . $d->format("d.m.");
        }
        public function renderContent() {
                //$debug = "<span style='font-size:50%'> (" . $this->dm() . "," . $this->id . ")</span>";
                $debug = '';
                return $debug;
        }
}


class Bereitschaft {
        public $fahrer;
        public $tage = array();

        public function score () {
                $az_tage = count($this->tage);
                $specialScore = ($az_tage * $az_tage) + 15;
                $name = $this->fahrer->name;
                if (array_key_exists($name,Plan::$specialParticipants)) {
                        return $specialScore + Plan::$specialParticipants[$name];
                }
                $now = new DateTimeImmutable('now');
                $deadline = new DateTimeImmutable('today');
                $deadline = $deadline->add(new DateInterval(Plan::$dailySwitch));

                $start_tag = $now->diff($deadline)->invert; //invert ist 1 wenn Zeitspanne negativ, sonst 0

                //am höchsten gewertet wird der zeitnächste eintrag, gewichtet mit der art des eintrags
                $weight = array( '+' => 7, '~' => 3, '-' => 1, '_' => 0);
                for( $i=$start_tag; $i < $az_tage; $i++) {
                        if ($this->tage[$i]->frueh) {
                                continue;
                        }
                        $dayScore = ($az_tage * ($az_tage - $i)) ;
                        return $dayScore + $weight[$this->tage[$i]->frueh];
                }
                return 0;
        }
}

class Plan {
        //Maximale Länge für Hinweistext (nicht static wg. json-encoding)
        public $hinweisMaxLen = 250;
        //Die Tageszeit nach der die Sortierreihenfolge der Teilnehmer von Einträgen der Folgetage bestimmt wird
        //d.h. der aktuelle Tag nicht mehr berücksichtigt wird
        public static $dailySwitch = 'PT17H00M';
        //Die Zeit in Tagen nach der inaktive Teilnehmer gelöscht werden
        public static $lastActionDelta = 365;
        //Einträge in $specialParticipants werden immer nach oben sortiert in der Reihenfolge die durch die Zahlen bestimmt ist.
        //Außerdem werden sie beim ersten Start automatisch angelegt und nach Ablauf der automatischen Löschfrist nicht gelöscht.
        public static $specialParticipants = array(
                'Lukas' => 1,
                'Sonja' => 1,
                'Marco' => 1,
                'Anna'  => 1,
        );
        //Statische Ressourcen mit Ampelstatus. Wenn nicht benötigt, das Array leer initialisieren: ...= array();
        public static $initialStaticResourcesWithStatus = array();
        //(wiederkehrende) Wochentage an denen Veranstaltungen z.B. Schleppbetrieb stattfinden können
        private static $weekdays = [
                'Wednesday',
        ];
        private static $file = "data/bereitschaften";
        private static $maxFahrer = 250;   //Maximale Anzahl an Teilnehmern
        private static $anzahlTage   = 14;
        private static $anzahlFahrer = 50; //Für Tests mit zufallsgenerierten Fahrern: deren Anzahl

        public $staticResourcesWithStatus;
        public $error= false;
        public $errorMsg ="";
        public $bereitschaften = array();
        public $referenzBereitschaft;
        public $aktuelleHinweise;
        public $aktuelleHinweiseDateTime;
        private $nextTagId = 0;
        private $nextFahrerId = 0;

        private static function today () {
                return new DateTimeImmutable('today -7 days');
        }
        private static function cmpBereitschaften($a,$b) {
                $c = $b->score() - $a->score();
                if ($c == 0) {
                        return strcasecmp($a->fahrer->name,$b->fahrer->name);
                }
                return $c;
        }
        public function staticResourceChanged($resourceID,$resourceValue) {
                $accept = array('+','-','~');
                if (in_array($resourceValue,$accept,true)) {
                        $this->staticResourcesWithStatus[$resourceID] = $resourceValue;
                        $this->speichern();
                }
        }
        public function hinweis($text) {
                if (strlen($text) > $this->hinweisMaxLen) {
                        $this->error = true;
                        $this->errorMsg = "Der Text ist zu lang.";
                        return;
                } else {
                        $this->aktuelleHinweise = preg_replace("/[\n\r]/","",$text);
                        $dt = new DateTimeImmutable('now');
                        $this->aktuelleHinweiseDateTime = $dt->format(DateTime::ATOM);
                        $this->speichern();
                }
        }
        public function getHinweisDateTimeFormatted() {
                if ($this->aktuelleHinweiseDateTime) {
                        $dt = DateTime::createFromFormat(DateTime::ATOM,$this->aktuelleHinweiseDateTime);
                        return $dt->format("d.m.Y H:i\h");
                } else {
                        return "";
                }

        }
        public function maxFahrername() {
                $max = 0;
                for ($i=0; $i < count($this->bereitschaften); $i++) {
                        $len = strlen($this->bereitschaften[$i]->fahrer->name);
                        if ($len > $max) {
                                $max = $len;
                        }
                }
                return $max;
        }
        public function update ($fahrerId,$datum,$spaet,$frueh) {
                for ($i=0; $i < count($this->bereitschaften); $i++) {
                        if ($fahrerId == $this->bereitschaften[$i]->fahrer->id) {
                                $this->bereitschaften[$i]->fahrer->lastAction(Plan::today());
                                for ($j=0; $j < count($this->bereitschaften[$i]->tage); $j++) {
                                        if ($datum == $this->bereitschaften[$i]->tage[$j]->datum) {
                                                $this->bereitschaften[$i]->tage[$j]->frueh = $frueh;
                                                $this->bereitschaften[$i]->tage[$j]->spaet = $spaet;
                                                break 2;
                                        }
                                }
                        }
                }
                $this->sort();
                $this->speichern();
        }

        private function pruneInactiveParticipants () {
                $oldestAcceptableAction = Plan::today()->modify("-" . Plan::$lastActionDelta . " days");
                $this->bereitschaften = array_values(array_filter($this->bereitschaften,function ($b) use($oldestAcceptableAction) {
                        if (array_key_exists($b->fahrer->name,Plan::$specialParticipants)) {
                                return true;
                        }

                        if ($b->fahrer->lastActionAsDate() < $oldestAcceptableAction) {
                                return false;
                        }
                        return true;
                }));
        }

        private static function weekdayNumOf($date) {
                return $date->format('w');
        }
        private static function weekdayOf($date) {
                return $date->format('l');
        }
        private static function numOfWeekday($weekday) {
                switch($weekday) {
                case 'Sunday'    :return 0;
                case 'Monday'    :return 1;
                case 'Tuesday'   :return 2;
                case 'Wednesday' :return 3;
                case 'Thursday'  :return 4;
                case 'Friday'    :return 5;
                case 'Saturday'  :return 6;
                default: throw new Exception("[" . $weekday . "] is not a weekday");
                }
        }
        private static function cmpWeekdays ($a,$b) {
                return Plan::numOfWeekday($a) - Plan::numOfWeekday($b);
        }
        private function getNextWeekdayFrom($datum,$weekdays) {
                usort($weekdays,array($this,"cmpWeekdays"));
                $day = Plan::weekdayOf($datum);
                if (!in_array($day,$weekdays,true)) {
                        for ($i=0; $i < count($weekdays); $i++) {
                                if (Plan::numOfWeekday($weekdays[$i]) > Plan::numOfWeekday($day)) {
                                        return $weekdays[$i];
                                }
                        }
                        return $weekdays[0];
                }
                return $day;
        }
        private function getWeekdaysAfter($datum) {
                $wd = Plan::$weekdays;
                usort($wd,array($this,"cmpWeekdays"));
                $weekday = $this->getNextWeekdayFrom($datum,$wd);

                while ($wd[0] != $weekday) {
                        array_push($wd, array_shift($wd));
                }

                return $wd;
        }
        public function fillDays($bereitschaft,$datum) {
                $weekdays = $this->getWeekdaysAfter($datum);
                $weekdayIdx = 0;
                $nWeekdays = count($weekdays);
                $datum = $datum->modify("-1 day");//allows to always use "next 'weekdayname'" in the loop
                while (count($bereitschaft->tage) < Plan::$anzahlTage) {
                        $datum = $datum->modify("next " . $weekdays[$weekdayIdx]);
                        $tag = new Tag($datum);
                        $tag->setId($this->nextTagId++);
                        array_push($bereitschaft->tage,$tag);
                        $weekdayIdx = ($weekdayIdx + 1) % $nWeekdays;
                }
        }
        private function pruneAndSupplementStandbyStatus () {

                $today = Plan::today();
                $nextStartDay = null;
                $nextWeekday = Plan::getNextWeekdayFrom($today,Plan::$weekdays);
                if (Plan::weekdayOf($today) == $nextWeekday) {
                        $nextStartDay = $today;
                } else {
                        $nextStartDay = $today->modify('next ' . $nextWeekday);
                }

                for ($i=0; $i < count($this->bereitschaften); $i++) {
                        while (count($this->bereitschaften[$i]->tage) > 0 && $this->bereitschaften[$i]->tage[0]->asDate() < $nextStartDay) {
                                array_shift($this->bereitschaften[$i]->tage);
                        }
                        $rest_tage = count($this->bereitschaften[$i]->tage);
                        if ($rest_tage > 0) {
                                $datum = $this->bereitschaften[$i]->tage[$rest_tage - 1]->asDate()->modify("+1 day");
                        } else {
                                $datum = $today;
                        }
                        $this->filldays($this->bereitschaften[$i],$datum);
                }
        }
        public function prune () {
                $this->pruneInactiveParticipants();
                $this->pruneAndSupplementStandbyStatus();
        }
        public function fahrerLoeschen ($fahrerId) {
                $this->bereitschaften = array_values(array_filter($this->bereitschaften,function ($b) use($fahrerId) {
                        if ($b->fahrer->id == $fahrerId) {
                                return false;
                        }
                        return true;
                }));
                $this->speichern();
        }
        public function fahrerAendern ($fahrerId,$fahrerName,$windenfahrerGS,$windenfahrerHG,$EWF) {
                if (!$this->checkName($fahrerName)) {
                        return;
                }
                for ($i=0; $i < count($this->bereitschaften); $i++) {
                        $fahrer = $this->bereitschaften[$i]->fahrer;
                        if ($fahrer->name == $fahrerName && $fahrer->id != $fahrerId) {
                                $this->error = true;
                                $this->errorMsg = "Der Name ist bereits vergeben";
                                return;
                        }
                }

                for ($i=0; $i < count($this->bereitschaften); $i++) {
                        if ($fahrerId == $this->bereitschaften[$i]->fahrer->id) {
                                $fahrer = $this->bereitschaften[$i]->fahrer;
                                $fahrer->lastAction(Plan::today());
                                if (isset($fahrerName) && $fahrerName != '' ) {
                                        $fahrer->name = $fahrerName;
                                }
                                $fahrer->windenfahrerGS = isset($windenfahrerGS) ? true : false;
                                $fahrer->windenfahrerHG = isset($windenfahrerHG) ? true : false;
                                $fahrer->EWF = isset($EWF) ? true : false;
                        }
                }
                $this->speichern();
        }
        public function checkName ($fahrerName) {
                if ( !(strlen($fahrerName)>=3 && strlen($fahrerName)<=25) || !preg_match('/^[-_0-9\p{L}\s\'\.]+$/u',$fahrerName)) {
                        $this->error = true;
                        $this->errorMsg = "Der Name ist ung&uuml;ltig";
                        return false;
                }
                return true;
        }
        public function neuerFahrer ($fahrerName,$windenfahrerGS,$windenfahrerHG,$EWF) {
                if (!$this->checkName($fahrerName)) {
                        return;
                }
                if (count($this->bereitschaften) >= Plan::$maxFahrer) {
                        $this->error = true;
                        $this->errorMsg =  "Die maximale Anzahl an Teilnehmern ist erreicht";
                        return;
                }
                for ($i=0; $i < count($this->bereitschaften); $i++) {
                        $fahrer = $this->bereitschaften[$i]->fahrer;
                        if ($fahrer->name == $fahrerName) {
                                $this->error = true;
                                $this->errorMsg = "Der Name ist bereits vergeben";
                                return;
                        }
                }
                $fahrer = new Fahrer($fahrerName,$this->nextFahrerId++);
                $fahrer->lastAction(Plan::today());
                $fahrer->windenfahrerGS = isset($windenfahrerGS) ? true : false;
                $fahrer->windenfahrerHG = isset($windenfahrerHG) ? true : false;
                $fahrer->EWF = isset($EWF) ? true : false;
                $bereitschaft = new Bereitschaft();
                $bereitschaft->fahrer = $fahrer;

                $this->fillDays($bereitschaft,Plan::today());
                array_push($this->bereitschaften,$bereitschaft);
                $this->speichern();
        }
        public function addSpecialParticipants() {
                $i=0;
                foreach(Plan::$specialParticipants as $special => $val) {
                        $this->bereitschaften[$i] = new Bereitschaft();
                        $this->bereitschaften[$i]->fahrer = new Fahrer($special,$this->nextFahrerId++);
                        $this->filldays($this->bereitschaften[$i],Plan::today());
                        $i++;
                }
        }
        public function random () {
                $status = array('+','-','~');
                $full = true;

                $daysBack = ($full ? 0 : rand(0,6));
                for( $i=0; $i < Plan::$anzahlFahrer; $i++) {
                        $this->bereitschaften[$i] = new Bereitschaft();
                        $this->bereitschaften[$i]->fahrer = new Fahrer(sprintf ("%'.04d Fahrer", $i),$this->nextFahrerId++);

                        $datum = Plan::today()->modify("-" . $daysBack . " days");
                        $this->bereitschaften[$i]->fahrer->lastAction($datum);
                        if ($full || rand(0,1)) {
                                $this->bereitschaften[$i]->fahrer->windenfahrerGS = rand(0,1);
                                $this->bereitschaften[$i]->fahrer->windenfahrerHG = rand(0,1);
                                $this->bereitschaften[$i]->fahrer->EWF = rand(0,1);
                        }
                        $datum = new DateTimeImmutable('-' . $daysBack . ' days');
                        $hasEntries = $full || rand(0,1);
                        for( $j=0; $j < Plan::$anzahlTage; $j++) {
                                $tag = new Tag($datum);
                                $tag->setId($this->nextTagId++);
                                $this->bereitschaften[$i]->tage[$j] = $tag;

                                if ($hasEntries && ($full || rand(0,1))) {
                                        $this->bereitschaften[$i]->tage[$j]->frueh = $status[$i < 10 ? 0 : rand(0,2)];
                                        $this->bereitschaften[$i]->tage[$j]->spaet = $status[$i < 10 ? 0 : rand(0,2)];
                                }
                                $datum = $datum->modify("+24 hours");
                        }
                }
                $this->referenzBereitschaft = new Bereitschaft();
                $this->fillDays($this->referenzBereitschaft,Plan::today()->modify("-" . $daysBack . " days"));

                $this->sort();
                $this->speichern();
        }
        public function resetIds() {
                $this->nextTagId = 0;
                for ($i=0; $i < count($this->bereitschaften); $i++) {
                        for ($j=0; $j < count($this->bereitschaften[$i]->tage); $j++) {
                                $this->bereitschaften[$i]->tage[$j]->setId($this->nextTagId++);
                        }
                }
        }
        public function sort () {
                usort($this->bereitschaften,array($this,"cmpBereitschaften"));
                $this->resetIds();
        }
        public function speichern () {
                file_put_contents(Plan::$file,serialize($this),LOCK_EX);
                file_put_contents("data/json",json_encode($this,JSON_PRETTY_PRINT),LOCK_EX);
        }
        public static function laden() {
                if( file_exists(Plan::$file)) {
                        $plan = unserialize(file_get_contents(Plan::$file,LOCK_EX));
                        $plan->prune();
                } else {
                        $plan = new Plan();
                        $plan->addSpecialParticipants();
                        $plan->speichern();
                }
                $plan->referenzBereitschaft = new Bereitschaft();
                $plan->fillDays($plan->referenzBereitschaft,Plan::today());

                $plan->sort();

                if(! isset($plan->staticResourcesWithStatus)
                   || count(array_diff_key(Plan::$initialStaticResourcesWithStatus,$plan->staticResourcesWithStatus)) > 0
                   || count(array_diff_key($plan->staticResourcesWithStatus,Plan::$initialStaticResourcesWithStatus)) > 0) {
                        $plan->staticResourcesWithStatus = Plan::$initialStaticResourcesWithStatus;
                }
                return $plan;
        }
        public function asJson() {
                return json_encode($this,JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS|JSON_HEX_QUOT);
        }
        public function asJsonToClient() {
                header('Content-Type: application/json');
                echo json_encode($this,JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS|JSON_HEX_QUOT|JSON_PRETTY_PRINT);
        }
}
